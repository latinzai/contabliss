﻿namespace ContaBLISS.Libraries
{
    using Data;
    using System;
    using Data.Models;
    using System.Collections.Generic;

    public abstract class ViewModel<T> : ValidableEntity  where T : ValidableEntity
    {
        private readonly IBusinessContext<T> context;

        public ViewModel(IBusinessContext<T> context)
        {
            this.context = context;
        }

        protected IBusinessContext<T> Context
        {
            get { return this.context; }
        }
        
    }
}