﻿using System.Collections.Generic;

namespace ContaBLISS.Libraries
{
    public interface ICRUDWindow<T>
    {
        void Create();

        ICollection<T> List();

        void Update();

        void Delete();
    }
}
