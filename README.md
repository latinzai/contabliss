# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* ContaBLISS is an ERP software built in .NET, which gives the user the ability to manage ledger accounts and their transactions.
* V1

### How do I get set up? ###

* Install DevExpress WPF and Reports.
* Make sure SQL Server is installed.
* From the ContaBLISS.Data project, run the Update-Database command.
* Run aspnet_regsql.exe and install the resources on your SQL Server instance. This is to enable Membership Provider.
* Start the program using the hardcoded credentials in the Login codebehind file.
