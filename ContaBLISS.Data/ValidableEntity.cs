﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;

namespace ContaBLISS.Data
{
    public abstract class ValidableEntity : IDataErrorInfo, INotifyPropertyChanged
    {
        public abstract object GetID();
        public string this[string columnName]
        {
            get
            {
                return OnValidate(columnName);
            }
        }

        [Obsolete]
        [Browsable(false)]
        [System.ComponentModel.DataAnnotations.Schema.NotMapped]
        public string Error
        {
            get
            {
                return "";
            }
        }

        [Browsable(false)]
        [System.ComponentModel.DataAnnotations.Schema.NotMapped]
        public bool ImValid { get; set; }

        
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual string OnValidate(string propertyName)
        {
            var context = new ValidationContext(this)
            {
                MemberName = propertyName
            };

            var results = new Collection<ValidationResult>();
            this.ImValid = Validator.TryValidateObject(this, context, results, true);
            NotifyPropertyChanged("ImValid");

            if (!ImValid)
            {
                ValidationResult result = results.SingleOrDefault(p =>
                                                                  p.MemberNames.Any(memberName =>
                                                                                    memberName == propertyName));
                return result == null ? null : result.ErrorMessage;
            }
            return null;
        }

        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public bool ValidateModel()
        {
            var context = new ValidationContext(this, serviceProvider: null, items: null);
            var results = new Collection<ValidationResult>();

            return Validator.TryValidateObject(this, context, results, true);
        }

    }
}
