﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContaBLISS.Data.CustomAnnotations
{
    public class NotEmpty : ValidationAttribute
    {
        private readonly int _minElements;

        public NotEmpty(int minElements)
        {
            _minElements = minElements;
        }
        public override bool IsValid(object value)
        {
            var list = value as ICollection;
            if (list != null)
            {
                return list.Count >= _minElements;
            }
            return false;
        }
    }
}
