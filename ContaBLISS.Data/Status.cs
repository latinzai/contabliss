﻿namespace ContaBLISS.Data
{
    using System.ComponentModel.DataAnnotations;

    public enum Status
    {
        [Display(Name = "Activo")]
        Active = 1,
        [Display(Name = "Inactivo")]
        Inactive = 0
    }

}