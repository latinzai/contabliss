﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ContaBLISS.Data.Models
{
    public class CurrencyRate
    {
        [Browsable(false)]
        [Key, ForeignKey("Currency"), Column(Order = 1)]
        public string CurrencyId { get; set; }
        public virtual Currency Currency { get; set; }
        [Key, Column(Order = 2)]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "dd/MM/yyyy")]
        public DateTime Date { get; set; }
        public decimal Rate { get; set; }

        public override string ToString()
        {
            return this.Rate.ToString();
        }
    }
}
