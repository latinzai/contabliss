﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ContaBLISS.Data.Models
{
    public class Period
    {
        [Key]
        [Browsable(false)]
        public int Id { get; set; }
        public DateTime BegDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
