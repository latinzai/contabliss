﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;

namespace ContaBLISS.Data.Models
{
    public class Currency : ValidableEntity
    {
        [Key]
        [MaxLength(3, ErrorMessage = "El código sólo puede ser de tres dígitos")]
        [Required(ErrorMessage = "Debe especificar el código ISO para la moneda")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string ISOCode { get; set; }
        [Required(ErrorMessage = "La moneda necesita una descripción")]
        public string Description { get; set; }
        [Required(ErrorMessage = "La moneda necesita un símbolo")]
        public string Simbol { get; set; }
        public Status Status { get; set; }

        public override string ToString()
        {
            return this.ISOCode;
        }

        public override object GetID()
        {
            return ISOCode;
        }
    }
}
