﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ContaBLISS.Data.Models
{
    public class AccountBalance : ValidableEntity
    {
        public AccountBalance()
        {
            LastModified = DateTime.Now;
        }

        [Browsable(false)]
        [Required]
        [Key, ForeignKey("Account")]
        public int AccountId { get; set; }
        public decimal Amount { get; set; }
        public DateTime LastModified { get; set; }

        [Browsable(false)]
        [Required]
        [ForeignKey("AmountCurrency")]
        public string CurrencyId { get; set; }

        public virtual Currency AmountCurrency { get; set; }
        public virtual Account Account { get; set; }

        public override string ToString()
        {
           if(AmountCurrency != null)
            return string.Format("{2}{0} [{1}]", this.Amount.ToString("#,##0.00"), this.CurrencyId, this.AmountCurrency.Simbol);
           else
            return string.Format("{0} [{1}]", this.Amount.ToString("#,##0.00"), this.CurrencyId);
        }

        public override object GetID()
        {
            return this.AccountId;
        }
    }
}
