﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System;

namespace ContaBLISS.Data.Models
{
    public class AccountType : ValidableEntity
    {
        [Key]
        [Browsable(false)]
        public int Id { get; set; }
        [Required(ErrorMessage = "El campo es obligatorio")]
        [MinLength(length:3, ErrorMessage = "La descripción debe ser de al menos 3 caracteres")]
        [MaxLength(length: 50, ErrorMessage = "La descripción no debe tener más de 50 caracteres")]
        public string Description { get; set; }
        public virtual Origin Origin { get; set; }
        public Status Status { get; set; }

        public override string ToString()
        {
            return this.Description;
        }

        public override object GetID()
        {
            return Id;
        }
    }
}