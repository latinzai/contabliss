﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ContaBLISS.Data.Models
{
    public class Entry : ValidableEntity
    {
        [Key]
        [Browsable(false)]
        public int Id { get; set; }
        public decimal Amount { get; set; }

        [Browsable(false)]
        [ForeignKey("Account")]
        public int AccountId { get; set; }

        [Browsable(false)]
        [ForeignKey("Transaction")]
        public int TransactionId { get; set; }

        [Browsable(false)]
        [ForeignKey("Currency")]
        public string CurrencyId { get; set; }

        public bool IsProcessed { get; set; }

        public virtual Account Account { get; set; }
        public virtual Transaction Transaction { get; set; }
        public virtual Origin Origin { get; set; }
        public virtual Currency Currency { get; set; }

        public override string ToString()
        {
            string operation = string.Empty;
            string str = string.Empty;

            if (Account.Type.Origin == this.Origin)
                operation = "+";
            else
                operation = "-";

            if (Id > 0)
                str = "{0} - [{1, -25} : {4}{2} {3}]";
            else
                str = "[{1, -25} : {4}{2} {3}]";

            return string.Format(str, Id, Account.Description.ToString(), Amount.ToString("#,##0.00"), Currency.ISOCode, operation);
        }

        public override object GetID()
        {
            return Id;
        }
    }
}
