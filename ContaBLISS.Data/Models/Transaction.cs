﻿using ContaBLISS.Data.CustomAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;


namespace ContaBLISS.Data.Models
{
    public class Transaction : ValidableEntity
    {
        public Transaction()
        {
            this.Entries = new HashSet<Entry>();
            this.Date = DateTime.Now;
        }

        [Key]
        [Browsable(false)]
        public int Id { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public DateTime Date { get; set; }
        public Status Status { get; set; }

        [NotEmpty(2, ErrorMessage = "Inserte al menos dos entradas")]
        public virtual ICollection<Entry> Entries { get; set; }

        public decimal TotalCredit
        {
            get
            {      
                return Entries.Where(e => e.Origin == Origin.CR).Select(e => e.Amount).Sum();
            }
        }

        public decimal TotalDebit
        {
            get
            {
                return Entries.Where(e => e.Origin == Origin.DB).Select(e => e.Amount).Sum();
            }
        }

        public bool IsBalanced
        {
            get {  return this.TotalCredit - this.TotalDebit == 0;  }
        }

        public override string ToString()
        {
            return string.Format("{0} [{1}]", this.Description, this.Date.ToString());
        }

        public override object GetID()
        {
            return Id;
        }

    }
}
