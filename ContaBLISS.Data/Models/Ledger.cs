﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ContaBLISS.Data.Models
{
    public class Ledger
    {
        [Key, Column(Order = 1)]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "dd/MM/yyyy")]
        public DateTime Date { get; set; }
        [Key, ForeignKey("Account"), Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AccountId { get; set; }

        public Account Account { get; set; }
        public ICollection<LedgerDetail> Details { get; set; }
    }
}
