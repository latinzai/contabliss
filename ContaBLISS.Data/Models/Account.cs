﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ContaBLISS.Data.Models
{
    public class Account : ValidableEntity
    {
        public Account()
        {
        }


        [Key]
        [Browsable(false)]
        public int Id { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Index("IX_AccountNumber", IsUnique = true)]
        [Required]
        [MaxLength(30)]
        public string Number { get { return this.number; } set { number = value; NotifyPropertyChanged(); } }
        [Required]
        public string Description { get; set; }
        public bool AllowTransactions { get; set; }

        [Browsable(false)]
        [ForeignKey("Type")]
        [Required]
        public int AccountTypeId { get; set; }

        [Browsable(false)]
        [ForeignKey("GLAccount")]
        public int? GLAccountId { get; set; }

        public virtual AccountType Type { get; set; }
        public virtual Account GLAccount { get; set; }
        public virtual AccountBalance Balance { get; set; }
        public Status Status { get; set; }

        private string number;
       

        public override string ToString()
        {
            return this.Number.ToString() + " " + this.Description;
        }

        public override object GetID()
        {
            return Id;
        }

        public static List<Account> GetChildAccounts(Account account, Connection conn, List<Account> childAccounts = null)
        {
            if (childAccounts == null)
                childAccounts = new List<Account>();

            var children = conn.Accounts.Where(a => a.GLAccountId == account.Id).ToList();

            foreach (var acc in children)
            {
                if (acc.AllowTransactions)
                    childAccounts.Add(acc);
                else
                    childAccounts = GetChildAccounts(acc, conn, childAccounts);
            }

            return childAccounts;
        }

    }


}
