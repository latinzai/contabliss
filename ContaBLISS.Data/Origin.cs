﻿namespace ContaBLISS.Data
{
    using System.ComponentModel.DataAnnotations;

    public enum Origin
    {
        [Display(Name = "Débito")]
        DB = 1,
        [Display(Name = "Crédito")]
        CR = 2
    }


}