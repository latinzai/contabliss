﻿namespace ContaBLISS.Data
{
    using System.Collections.Generic;

    public interface IBusinessContext<T>
    {
        void Create(T entity);

        void Delete(T entity);
        
        void Update(T entity);
        
        ICollection<T> GetAll();
    }
}