﻿
using ContaBLISS.Data.Models;
using System.Data.Entity;
//using System.Data.Entity.ModelConfiguration.Conventions;

namespace ContaBLISS.Data
{
    public class Connection : DbContext
    {
        public Connection(): base("DefaultConnection")
        {

        }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<AccountType> AccountTypes { get; set; }
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<Period> Periods { get; set; }
        public DbSet<CurrencyRate> CurrencyRates { get; set; }
        public DbSet<Entry> Entries { get; set; }
        public DbSet<AccountBalance> AccountBalances { get; set; }
        public DbSet<LedgerDetail> LedgerDetails { get; set; }
        public DbSet<Ledger> Ledgers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>().HasOptional(a => a.GLAccount).WithMany().HasForeignKey(a => a.GLAccountId);
//            modelBuilder.Entity<Account>().HasOptional(a => a.GLAccount).WithOptionalPrincipal().Map(a => a.MapKey("GLAccountId"));
            base.OnModelCreating(modelBuilder);
        }
    }
}
