namespace ContaBLISS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DbInit : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccountBalances",
                c => new
                    {
                        AccountId = c.Int(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LastModified = c.DateTime(nullable: false),
                        CurrencyId = c.String(nullable: false, maxLength: 3),
                    })
                .PrimaryKey(t => t.AccountId)
                .ForeignKey("dbo.Accounts", t => t.AccountId)
                .ForeignKey("dbo.Currencies", t => t.CurrencyId, cascadeDelete: true)
                .Index(t => t.AccountId)
                .Index(t => t.CurrencyId);
            
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Number = c.String(nullable: false, maxLength: 30),
                        Description = c.String(nullable: false),
                        AllowTransactions = c.Boolean(nullable: false),
                        AccountTypeId = c.Int(nullable: false),
                        GLAccountId = c.Int(),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Accounts", t => t.GLAccountId)
                .ForeignKey("dbo.AccountTypes", t => t.AccountTypeId, cascadeDelete: true)
                .Index(t => t.Number, unique: true, name: "IX_AccountNumber")
                .Index(t => t.AccountTypeId)
                .Index(t => t.GLAccountId);
            
            CreateTable(
                "dbo.AccountTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 50),
                        Origin = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Currencies",
                c => new
                    {
                        ISOCode = c.String(nullable: false, maxLength: 3),
                        Description = c.String(nullable: false),
                        Simbol = c.String(nullable: false),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ISOCode);
            
            CreateTable(
                "dbo.CurrencyRates",
                c => new
                    {
                        CurrencyId = c.String(nullable: false, maxLength: 3),
                        Date = c.DateTime(nullable: false),
                        Rate = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => new { t.CurrencyId, t.Date })
                .ForeignKey("dbo.Currencies", t => t.CurrencyId, cascadeDelete: true)
                .Index(t => t.CurrencyId);
            
            CreateTable(
                "dbo.Entries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AccountId = c.Int(nullable: false),
                        TransactionId = c.Int(nullable: false),
                        CurrencyId = c.String(maxLength: 3),
                        IsProcessed = c.Boolean(nullable: false),
                        Origin = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: true)
                .ForeignKey("dbo.Currencies", t => t.CurrencyId)
                .ForeignKey("dbo.Transactions", t => t.TransactionId, cascadeDelete: true)
                .Index(t => t.AccountId)
                .Index(t => t.TransactionId)
                .Index(t => t.CurrencyId);
            
            CreateTable(
                "dbo.Transactions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.LedgerDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        DebitAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreditAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Ledger_Date = c.DateTime(),
                        Ledger_AccountId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Ledgers", t => new { t.Ledger_Date, t.Ledger_AccountId })
                .Index(t => new { t.Ledger_Date, t.Ledger_AccountId });
            
            CreateTable(
                "dbo.Ledgers",
                c => new
                    {
                        Date = c.DateTime(nullable: false),
                        AccountId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Date, t.AccountId })
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: true)
                .Index(t => t.AccountId);
            
            CreateTable(
                "dbo.Periods",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BegDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LedgerDetails", new[] { "Ledger_Date", "Ledger_AccountId" }, "dbo.Ledgers");
            DropForeignKey("dbo.Ledgers", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.Entries", "TransactionId", "dbo.Transactions");
            DropForeignKey("dbo.Entries", "CurrencyId", "dbo.Currencies");
            DropForeignKey("dbo.Entries", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.CurrencyRates", "CurrencyId", "dbo.Currencies");
            DropForeignKey("dbo.AccountBalances", "CurrencyId", "dbo.Currencies");
            DropForeignKey("dbo.AccountBalances", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.Accounts", "AccountTypeId", "dbo.AccountTypes");
            DropForeignKey("dbo.Accounts", "GLAccountId", "dbo.Accounts");
            DropIndex("dbo.Ledgers", new[] { "AccountId" });
            DropIndex("dbo.LedgerDetails", new[] { "Ledger_Date", "Ledger_AccountId" });
            DropIndex("dbo.Entries", new[] { "CurrencyId" });
            DropIndex("dbo.Entries", new[] { "TransactionId" });
            DropIndex("dbo.Entries", new[] { "AccountId" });
            DropIndex("dbo.CurrencyRates", new[] { "CurrencyId" });
            DropIndex("dbo.Accounts", new[] { "GLAccountId" });
            DropIndex("dbo.Accounts", new[] { "AccountTypeId" });
            DropIndex("dbo.Accounts", "IX_AccountNumber");
            DropIndex("dbo.AccountBalances", new[] { "CurrencyId" });
            DropIndex("dbo.AccountBalances", new[] { "AccountId" });
            DropTable("dbo.Periods");
            DropTable("dbo.Ledgers");
            DropTable("dbo.LedgerDetails");
            DropTable("dbo.Transactions");
            DropTable("dbo.Entries");
            DropTable("dbo.CurrencyRates");
            DropTable("dbo.Currencies");
            DropTable("dbo.AccountTypes");
            DropTable("dbo.Accounts");
            DropTable("dbo.AccountBalances");
        }
    }
}
