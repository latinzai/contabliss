﻿
namespace ContaBLISS.Data
{
    using Data;
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class BusinessContext<T> : IBusinessContext<T>, IDisposable
        where T: ValidableEntity
    {
        private readonly DbContext context;
        private bool disposed;

        public BusinessContext() : this(new Connection()) { }

        public BusinessContext(DbContext context)
        {
            this.context = context;
        }

        public void Create(T entity)
        {
            if (entity.ValidateModel())
            {
                context.Set<T>().Add(entity);
                context.SaveChanges();
            }
        }

        public void Delete(T entity)
        {
            if(!IsTracked(entity))
                context.Set<T>().Attach(entity);
            context.Set<T>().Remove(entity);
            context.SaveChanges();
        }

        private bool IsTracked(T entity)
        {
            return context.ChangeTracker.Entries<T>().Any(e => e.Entity.GetID() == entity.GetID());
        }
        public void Update(T entity)
        {
            if (entity.ValidateModel())
            {
           
                var oldEntity = context.Set<T>().Find(entity.GetID());

                if (oldEntity == null)
                    throw new InvalidOperationException("Entity not found in database.");


                context.Entry<T>(oldEntity).CurrentValues.SetValues(entity);
                context.SaveChanges();
            }
        }

        public virtual System.Collections.Generic.ICollection<T> GetAll()
        {
            return context.Set<T>().ToList();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposed || !disposing)
                return;
            if (context != null)
                context.Dispose();

            disposed = true;
        }
    }

}
