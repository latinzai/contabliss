﻿using DevExpress.Xpf.Bars;
using DevExpress.Xpf.Docking;
using DevExpress.Xpf.Ribbon;
using System;
using System.Windows;

namespace ContaBLISS.Client.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : DXRibbonWindow
    {
        public MainWindow()
        {
            new Login().ShowDialog();

            InitializeComponent();

       
        }
        

        public void LoadPermissions()
        {
            switch (App.LoggedUser.Role)
            {
                case "CONTABLE":
                    btnUsers.IsEnabled = false;
                    btnRoles.IsEnabled = false;
                    btnTypes.IsEnabled = false;
                    btnCurrencies.IsEnabled = false;
                    break;
                case "AUDITOR":
                    btnUsers.IsEnabled = false;
                    btnRoles.IsEnabled = false;
                    btnTypes.IsEnabled = false;
                    btnCurrencies.IsEnabled = false;
                    break;
                default:
                    break;
            }
        }
      
        
        private async void OpenPanel(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            var button = sender as BarButtonItem;
            DocumentPanel doc = null;
            try
            {
                switch (button.Name)
                {
                    case "btnCurrencies":
                        doc = new CurrencyDocumentPanel();
                        break;
                    case "btnTypes":
                        doc = new AccountTypeDocumentPanel();
                        break;
                    case "btnCatalog":
                        doc = new AccountDocumentPanel();
                        break;
                    case "btnTrans":
                        doc = new TransactionsDocumentPanel();
                        break;
                    case "btnUsers":
                        doc = new UserDocumentPanel();
                        break;
                    case "btnRoles":
                        doc = new RoleDocumentPanel();
                        break;
                    case "btnLedger":
                        doc = new BalanceTrialDocumentPanel();
                        break;
                }


                docGroup.Clear();
                doc.IsActive = true;
                doc.MDIState = MDIState.Maximized;
                docGroup.Add(doc);
            }
            catch (NullReferenceException ex)
            {
                MessageBox.Show("TBD");
            }
            

        }

    }
}
