﻿namespace ContaBLISS.Client.Views
{
    using Data;
    using System;
    using System.Linq;
    using Data.Models;
    using Helpers;
    using System.Collections.Generic;
    using DevExpress.Xpf.Docking.Base;
    using DevExpress.Xpf.Editors;
    using System.Windows;
    using System.ComponentModel;
    using System.Collections.ObjectModel;

    public partial class TransactionsDocumentPanel : DevExpress.Xpf.Docking.DocumentPanel
    {
        public TransactionsDocumentPanel()
        {
            InitializeComponent();
            DataContext = this;
            btnAddCredit.Content = DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/add_16x16.png").ToWPFImage(System.Windows.Media.Stretch.Uniform);
            btnAddDebit.Content = DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/add_16x16.png").ToWPFImage(System.Windows.Media.Stretch.Uniform);
            this.Accounts = new ObservableCollection<Account>();
            this.DebitEntries = new ObservableCollection<Entry>();
            this.CreditEntries = new ObservableCollection<Entry>();
            this.SavedTransactions = new ObservableCollection<Transaction>();
            LoadSavedTransactions();
            LoadAccounts();
        }

        private Transaction _selectedTrans;
        public Transaction SelectedTransaction
        {
            get { return _selectedTrans;}
            set
            {
                _selectedTrans = value;
                LoadAccounts();
                if(value != null)
                {
                    DebitEntries.Clear();
                    CreditEntries.Clear();
                    value.Entries.Where(e => e.Origin == Origin.DB).ToList().ForEach(e => { DebitEntries.Add(e); RemoveAccountFromDroplist(e.Account); });
                    value.Entries.Where(e => e.Origin == Origin.CR).ToList().ForEach(e => { CreditEntries.Add(e); RemoveAccountFromDroplist(e.Account); });
                }

                UpdateTotalLabel();

            }
        }
        public ICollection<Transaction> SavedTransactions { get; set; }
        public ICollection<Account> Accounts { get; set; }

        public ICollection<Entry> DebitEntries { get; set; }

        public ICollection<Entry> CreditEntries { get; set; }

        public decimal TotalDebit
        {
            get
            {
                return this.DebitEntries.Sum(e => e.Amount);
            }
        }

        public decimal TotalCredit
        {
            get
            {
                return this.CreditEntries.Sum(e => e.Amount);
            }
        }

        private void UpdateTotalLabel()
        {
            lblTotalC.Content = "Total Crédito: " + TotalCredit.ToString("#,##0.00");
            lblTotalD.Content = "Total Débito: " + TotalDebit.ToString("#,##0.00");
        }

        private void LoadSavedTransactions()
        {
            using (var conn = new Connection())
            {
                conn.Transactions.Include("Entries.Account.Type").Include("Entries.Account.Balance").Include("Entries.Currency").ToList().ForEach(t => { this.SavedTransactions.Add(t); });
            }
        }
        private void LoadAccounts()
        {
            Accounts.Clear();  
            using (var conn = new Data.Connection())
            {
                conn.Accounts.Include("Balance.AmountCurrency").Include("Type").Where(a => a.Status == Status.Active && a.AllowTransactions).ToList().ForEach(a => { this.Accounts.Add(a); });

            }
        }

        private void AccountsChanged(object sender, DevExpress.Xpf.Editors.EditValueChangedEventArgs e)
        {
            ComboBoxEdit cmb = sender as ComboBoxEdit;

            if (cmbDebitAccount.EditValue == cmbCreditAccount.EditValue)
                cmb.SelectedIndex = -1;
        }



        private void ClearTransactionFields()
        {
            txtCreditAmount.Text = string.Empty;
            txtDebitAmount.Text = string.Empty;
            cmbCreditAccount.SelectedItem = null;
            cmbDebitAccount.SelectedItem = null;
            cmbDebitAccount.Text = string.Empty;
            cmbCreditAccount.Text = string.Empty;
            cmbCreditAccount.SelectedIndex = -1;
            cmbDebitAccount.SelectedIndex = -1;
        }

        private void ClearControls()
        {
            ClearTransactionFields();
            txtDescription.Text = string.Empty;
            txtDate.DateTime = DateTime.Now;
        }
        private void btnAddCredit_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            AddCreditEntry();
        }

        private void AddCreditEntry()
        {
            Account selectedAccount = cmbCreditAccount.EditValue as Account;
            if (string.IsNullOrWhiteSpace(txtCreditAmount.Text))
                txtCreditAmount.Text = "0";
            decimal creditAmount = decimal.Parse(txtCreditAmount.Text);
            if (creditAmount <= 0)
            {
                MessageBox.Show("El monto debe ser mayor que 0", "Agregar Débito");
                return;
            }
            if (selectedAccount.Type.Origin != Origin.CR && selectedAccount.Balance.Amount < creditAmount)
            {
                MessageBox.Show("La cuenta no tiene balance suficiente", "Agregar Crédito");
                return;
            }
            else
            {
                var entry = new Entry()
                {
                    Origin = Origin.CR,
                    Account = selectedAccount,
                    AccountId = selectedAccount.Id,
                    Currency = selectedAccount.Balance.AmountCurrency,
                    CurrencyId = selectedAccount.Balance.CurrencyId,
                    Amount = creditAmount

                };

                CreditEntries.Add(entry);
                RemoveAccountFromDroplist(selectedAccount);
                ClearTransactionFields();
                UpdateTotalLabel();
            }

        }

        private IEnumerable<Entry> GetEntriesToSave()
        {
            var entries = DebitEntries.Concat(CreditEntries);
            var cleanEntries = from e in entries
                               select new Entry
                               {
                                   AccountId = e.AccountId,
                                   CurrencyId = e.Account.Balance.CurrencyId,
                                   Amount = e.Amount,
                                   Origin = e.Origin
                               };
            return cleanEntries;
        }
        private void Create()
        {
            if (TotalDebit == TotalCredit)
            {
                Transaction trans = new Transaction
                {
                    Date = txtDate.DateTime,
                    Description = txtDescription.Text,
                    Entries = GetEntriesToSave().ToList()
                };

                using (var context = new BusinessContext<Transaction>())
                {
                    try
                    {
                        if(trans.ValidateModel())
                        {
                        context.Create(trans);
                        //obtener las transacctiones con los navigation properties no nulos.
                       
                        trans.Entries = DebitEntries.Concat(CreditEntries).ToList();
                        UpdateBalances(trans.Entries);
                        LoadSavedTransactions();
                        RefreshScreen();
                        }
                        else
                        {
                            MessageBox.Show("Favor llene los campos obligatorios");
                        }

                    }
                    catch (Exception e)
                    {
                        MessageBox.Show("Ha ocurrido un error: " + e.GetBaseException().Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            else
            {
                MessageBox.Show(string.Format("Las transacciones no están compensadas. DB:{0} ≠ CR:{1}", TotalDebit.ToString("#,##0.00"), TotalCredit.ToString("#,##0.00")));
            }
        } 

        private void AddDebitEntry()
        {
            Account selectedAccount = cmbDebitAccount.EditValue as Account;
            if (string.IsNullOrWhiteSpace(txtDebitAmount.Text))
                txtDebitAmount.Text = "0";
          
            decimal debitAmount = decimal.Parse(txtDebitAmount.Text);
            if (debitAmount <= 0)
            {
                MessageBox.Show("El monto debe ser mayor que 0", "Agregar Crédito");
                return;
            }
            if (selectedAccount.Type.Origin != Origin.DB && selectedAccount.Balance.Amount < debitAmount)
            {
                MessageBox.Show("La cuenta no tiene balance suficiente", "Agregar Crédito");
            }
            else
            {
                var entry = new Entry()
                {
                    Origin = Origin.DB,
                    Account = selectedAccount,
                    AccountId = selectedAccount.Id,
                    Currency = selectedAccount.Balance.AmountCurrency,
                    CurrencyId = selectedAccount.Balance.CurrencyId,
                    Amount = debitAmount

                };

                DebitEntries.Add(entry);
                RemoveAccountFromDroplist(selectedAccount);
                ClearTransactionFields();
                UpdateTotalLabel();

            }
        }
        private void RemoveAccountFromDroplist(Account account)
        {
            this.Accounts.RemoveWhere(x => x.Id == account.Id);
       }
        private void btnAddDebit_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            AddDebitEntry();
          
        }

        private void DeleteSelectedEntry(object sender, System.Windows.Input.KeyEventArgs e)
        {
            ListBoxEdit list = sender as ListBoxEdit;

            if(e.Key == System.Windows.Input.Key.Delete && list.SelectedIndex != -1)
            {
                Entry selectedEntry = list.SelectedItem as Entry;
                Accounts.Add(selectedEntry.Account);
                if(!DebitEntries.Remove(selectedEntry))
                {
                    CreditEntries.Remove(selectedEntry);
                }
            }

            UpdateTotalLabel();
                
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Create();
            SelectedTransaction = null;
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            Delete();
        }

        private void Delete()
        {
            if (SelectedTransaction != null)
            {
                using (var context = new BusinessContext<Transaction>())
                {
                    try
                    {
                        context.Delete(SelectedTransaction);
                        SavedTransactions.Remove(SelectedTransaction);
                    }
                    catch(Exception e)
                    {
                        MessageBox.Show("Ha ocurrido un error: " + e.GetBaseException().Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                    }
                }

                RefreshScreen();
            }
        }

        private void RefreshScreen()
        {
            ClearControls();
            DebitEntries.Clear();
            CreditEntries.Clear();
            LoadAccounts();
            UpdateTotalLabel();

        }
        private void Update()
        {
            if(SelectedTransaction != null)
            {
                using (var context = new BusinessContext<Transaction>())
                {
                    context.Update(SelectedTransaction);
                }
            }
        }

        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            SelectedTransaction = null;
            RefreshScreen();
        }

        private void UpdateBalances(ICollection<Entry> entries)
        {
            foreach (var entry in entries)
            {
                UpdateBalance(entry);
            }
        }

        private void UpdateBalance(Entry entry)
        {
            using (var con = new BusinessContext<AccountBalance>())
            {
                var account = entry.Account;
                if (entry.Origin == account.Type.Origin)
                    account.Balance.Amount += entry.Amount;
                else
                    account.Balance.Amount -= entry.Amount;
                con.Update(account.Balance);
            }
        }
    }
}
