﻿namespace ContaBLISS.Client.Views
{
    using Data;
    using Libraries;
    using System.Linq;
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Reflection;

    /// <summary>
    /// Interaction logic for AccountTypeDocumentPanel.xaml
    /// </summary>
    public partial class AccountDocumentPanel : DevExpress.Xpf.Docking.DocumentPanel
    {
        public AccountDocumentPanel()
        {
            InitializeComponent();
            this.DataContext = new ViewModels.AccountViewModel();
            using (var conn = new Connection())
            {
                cmbType.ItemsSource = conn.AccountTypes.Where(t => t.Status == Status.Active).ToList();
                cmbCurrency.ItemsSource = conn.Currencies.Where(t => t.Status == Status.Active).ToList();

            }
        }

        private void GridControl_SelectedItemChanged(object sender, DevExpress.Xpf.Grid.SelectedItemChangedEventArgs e)
        {
            if (e.NewItem != null)
            {
                chkTrans.IsEnabled = false;

                if (((Data.Models.Account)e.NewItem).GLAccount == null)
                {
                    cmbGLAccount.Text = string.Empty;
                    cmbGLAccount.SelectedIndex = -1;
                }
            }
            else
            {
                chkTrans.IsEnabled = true;
            }

        }

        private void btnNew_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            cmbGLAccount.Text = string.Empty;
            cmbGLAccount.SelectedIndex = -1;
        }
    }


}
