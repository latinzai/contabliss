﻿namespace ContaBLISS.Client.Views
{
    using Libraries;
    
    public partial class CurrencyDocumentPanel : DevExpress.Xpf.Docking.DocumentPanel
    {
        public CurrencyDocumentPanel()
        {
            InitializeComponent();
            this.DataContext = new ContaBLISS.Client.ViewModels.CurrencyViewModel();

        }
    }
}
