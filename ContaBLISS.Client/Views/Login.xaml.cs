﻿using System.Web.Security;
using System.Windows;

namespace ContaBLISS.Client.Views
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            if (Membership.GetAllUsers().Count < 1)
            {
                Membership.CreateUser("user", "Abc123*");
            }
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {

            if (!Membership.ValidateUser(txtUsername.Text, txtPass.Password))
            {
                MessageBox.Show("Usuario o contraseña inválido");
            }
            else
            {
                App.LoggedUser = new UserWrapper(Membership.GetUser(txtUsername.Text), UserProfile.GetUserProfile((txtUsername.Text)));
                this.Close();
            }

        }
    }

}
