﻿namespace ContaBLISS.Client.Views
{
    using Data;
    using Libraries;
    using System.Linq;
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Reflection;

    /// <summary>
    /// Interaction logic for AccountTypeDocumentPanel.xaml
    /// </summary>
    public partial class AccountTypeDocumentPanel : DevExpress.Xpf.Docking.DocumentPanel
    {
        public AccountTypeDocumentPanel()
        {
            InitializeComponent();
            this.DataContext = new ViewModels.AccounTypeViewModel();
            cmbOrigin.ItemsSource = Enum.GetValues(typeof(ContaBLISS.Data.Origin)).Cast<Origin>();
        }
    }

    public static class EnumHelper
    {
        public static string DisplayName(this Enum value)
        {
            Type enumType = value.GetType();
            var enumValue = Enum.GetName(enumType, value);
            MemberInfo member = enumType.GetMember(enumValue)[0];

            var attrs = member.GetCustomAttributes(typeof(DisplayAttribute), false);
            var outString = ((DisplayAttribute)attrs[0]).Name;

            if (((DisplayAttribute)attrs[0]).ResourceType != null)
            {
                outString = ((DisplayAttribute)attrs[0]).GetName();
            }

            return outString;
        }
    }
}
