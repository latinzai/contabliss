﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using ContaBLISS.Data;
using System.Linq;
using ContaBLISS.Data.Models;
using DevExpress.XtraReports.UI;
using System.Data.Entity;
using DevExpress.LookAndFeel;
using System;

namespace ContaBLISS.Client.Views
{
    /// <summary>
    /// Interaction logic for RoleDocumentPanel.xaml
    /// </summary>
    public partial class BalanceTrialDocumentPanel : DevExpress.Xpf.Docking.DocumentPanel
    {
        public BalanceTrialDocumentPanel()
        {
            InitializeComponent();
            this.DataContext = this;
            this.Accounts = new ObservableCollection<Account>();
            this.SavedLedgers = new ObservableCollection<Ledger>();
            LoadAccounts();
            LoadLedgers();
        }

        private void LoadLedgers()
        {
            SavedLedgers.Clear();
            using (var conn = new Connection())
            {
                foreach (var ledger in conn.Ledgers.Include("Account").ToList())
                {         
                    this.SavedLedgers.Add(ledger);
                }
            }
        }

        public Account SelectedAccount { get; set; }
        public Ledger SelectedLedger { get; set; }
        public ICollection<Account> Accounts { get; set; }
        public ICollection<Ledger> SavedLedgers { get; set; }

        private void LoadAccounts()
        {
            using (var conn = new Connection())
            {
                foreach (var account in conn.Accounts.OrderBy(a => a.Number).ToList())
                {
                    this.Accounts.Add(account);
                }
            }
        }
        private void GenerateLedger()
        {
            if (MessageBox.Show("¿Está seguro de realizar la mayorización?", "Mayorización", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                try
                {
                    using (var conn = new Connection())
                    {
                        var details = new List<LedgerDetail>();
                        List<string> accountNumbers = new List<string>();
                        var ledgerDate = System.DateTime.Now;

                        if (SelectedAccount.AllowTransactions)
                        {
                            conn.Entries.Where(en => en.AccountId == SelectedAccount.Id && en.Transaction.Date >= txtDateBeg.DisplayDate && en.Transaction.Date <= txtDateEnd.DisplayDate && !en.IsProcessed).ToList().ForEach(en => {

                                details.Add(
                                    new LedgerDetail
                                    {
                                        CreditAmount = en.Origin == Origin.CR ? en.Amount : 0,
                                        DebitAmount = en.Origin == Origin.DB ? en.Amount : 0,
                                        Date = en.Transaction.Date
                                    });
                                en.IsProcessed = true;
                                conn.Entry<Entry>(en).State = EntityState.Modified;
                            });

                            accountNumbers.Add(SelectedAccount.Number);

                            conn.Ledgers.Add(
                                new Ledger { Date = ledgerDate, AccountId = SelectedAccount.Id, Details = details }
                            );
                        }
                        else
                        {
                            var childAccounts = Account.GetChildAccounts(SelectedAccount, conn);

                            foreach (var acc in childAccounts)
                            {
                                conn.Entries.Where(en => en.AccountId == acc.Id && en.Transaction.Date >= txtDateBeg.DisplayDate && en.Transaction.Date <= txtDateEnd.DisplayDate && !en.IsProcessed).ToList().ForEach(en => {

                                    details.Add(
                                        new LedgerDetail
                                        {
                                            CreditAmount = en.Origin == Origin.CR ? en.Amount : 0,
                                            DebitAmount = en.Origin == Origin.DB ? en.Amount : 0,
                                            Date = en.Transaction.Date
                                        }
                                        );
                                    en.IsProcessed = true;
                                    conn.Entry<Entry>(en).State = EntityState.Modified;
                                });

                                conn.Ledgers.Add(
                                    new Ledger { Date = ledgerDate, AccountId = acc.Id, Details = details }
                                    );
                               
                                accountNumbers.Add(acc.Number);
                                details.Clear();
                            }
                        }
                        
                        conn.SaveChanges();

                        OpenReport(accountNumbers.ToArray(), ledgerDate);

                        LoadLedgers();
                    }
                }
                catch (System.Data.Entity.Infrastructure.DbUpdateException ex)
                {
                    MessageBox.Show("Ya se ha mayorizado esta cuenta");
                }
                catch (System.Exception ex)
                {
                    MessageBox.Show("Ha ocurrido un error: " + ex.Message);

                }

            }
        }
        private void btnMayorizar_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            GenerateLedger();
        }

        private void OpenReport(string[] accountNumbers, DateTime date)
        {
            var report = new ContaBLISS.Client.Report.LedgerReport();
            report.Parameters["AccountNumber"].Value = accountNumbers;
            report.Parameters["DateBeg"].Value = date;
            report.Parameters["DateEnd"].Value = date;

            using (ReportPrintTool printTool = new ReportPrintTool(report))
            {

                printTool.ShowPreviewDialog();

                printTool.ShowPreview(UserLookAndFeel.Default);
            }
        }

        private void btnViewReport_Click(object sender, RoutedEventArgs e)
        {
            if(SelectedLedger != null)
            {
                OpenReport(new string[] { SelectedLedger.Account.Number }, SelectedLedger.Date);
            }
        }

    }
   
}
