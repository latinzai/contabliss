﻿using DevExpress.Xpf.Docking;
using System.Web.Security;
using System.Web.Profile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Collections.ObjectModel;
using ContaBLISS.Client.Helpers;
using DevExpress.XtraGrid.Views.Grid;

namespace ContaBLISS.Client.Views
{
    /// <summary>
    /// Interaction logic for UsersDocumentPanel.xaml
    /// </summary>
    public sealed partial class UserDocumentPanel : DocumentPanel
    {
        private static UserDocumentPanel _instance = null;

        public string SelectedUserRole { get; set; }
        public string SelectedRole { get; set; }
        public string SelectedUser { get; set; }
        public ICollection<UserWrapper> Users { get; set; }
        public ICollection<string> SavedRoles { get; set; }
        public ICollection<string> UserRoles { get; set; }

        public UserDocumentPanel()
        {
            InitializeComponent();
            this.DataContext = this;
            this.Users = new ObservableCollection<UserWrapper>();
            this.SavedRoles = new ObservableCollection<string>();
            this.UserRoles = new ObservableCollection<string>();
            LoadUsers();
        }

        public static UserDocumentPanel Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new UserDocumentPanel();
                }

                return _instance;
            }
        }

        private void DeleteUser()
        {
            if(!string.IsNullOrEmpty(SelectedUser))
            {
                try
                {
                    Membership.DeleteUser(SelectedUser);
                    ProfileManager.DeleteProfile(SelectedUser);
                    this.Users.RemoveWhere(u => u.Username == SelectedUser);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void CreateUser()
        {
            try
            {
                MembershipUser membership = Membership.CreateUser(txtUsername.Text.Trim(), txtPassword.Text, txtEmail.Text.Trim());
            

                UserProfile profile = UserProfile.GetUserProfile(txtUsername.Text.Trim());
                profile.FirstName = txtFirstName.Text.Trim();
                profile.LastName = txtLastName.Text.Trim();
                profile.Save();
                Users.Add(new UserWrapper(membership, profile));
                this.New();

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void New()
        {
            this.SelectedUser = string.Empty;
            txtFirstName.Clear();
            txtLastName.Clear();
            txtPassword.Clear();
            txtUsername.Clear();
            txtEmail.Clear();
        }
        private void AddRoles(string user, string[] roles)
        {
            try
            {
                Roles.AddUserToRoles(user, roles);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void RemoveRoles(string user, string[] roles)
        {
            try
            {
                Roles.RemoveUserFromRoles(user, roles);
            }
            catch (Exception e)
            {

                MessageBox.Show(e.Message);
            }
        }

        private void LoadUsers()
        {
            foreach (MembershipUser user in Membership.GetAllUsers())
                this.Users.Add(new UserWrapper(user, UserProfile.GetUserProfile(user.UserName)));
        }

        private void LoadUserRoles(string user)
        {
            this.UserRoles.Clear();
            Roles.GetRolesForUser(user).ToList().ForEach(r => { this.UserRoles.Add(r); });
        }
        
        private void LoadAllRoles()
        {
            SavedRoles.Clear();
            Roles.GetAllRoles().ToList().ForEach(r => { if (!this.UserRoles.Contains(r))this.SavedRoles.Add(r); });
        }

        private void LoadRoles()
        {
            LoadUserRoles(SelectedUser);
            LoadAllRoles();
        }

        private void btnAddRole_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(SelectedRole))
            {
                AddRoles(SelectedUser, new string[] { SelectedRole });
                UserRoles.Add(SelectedRole);
                SavedRoles.Remove(SelectedRole);
            }
        }

        private void btnRemoveRole_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(SelectedUserRole))
            {
                RemoveRoles(SelectedUser, new string[] { SelectedUserRole });
                SavedRoles.Add(SelectedUserRole);
                UserRoles.Remove(SelectedUserRole);
            }
        }

        private void cmbRoleUser_SelectedIndexChanged(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(SelectedUser))
            {
                LoadRoles();
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            CreateUser();
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            DeleteUser();   
        }

        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            New();
        }
    }

    public class UserProfile : ProfileBase
    {
        public static UserProfile GetUserProfile(string username)
        {
            return Create(username) as UserProfile;
        }
        public static UserProfile GetUserProfile()
        {
            return Create(Membership.GetUser().UserName) as UserProfile;
        }

        [SettingsAllowAnonymous(false)]
        public string FirstName
        {
            get { return base["FirstName"] as string; }
            set { base["FirstName"] = value; }
        }

        [SettingsAllowAnonymous(false)]
        public string LastName
        {
            get { return base["LastName"] as string; }
            set { base["LastName"] = value; }
        }
       
    }

    public class UserWrapper
    {
        private MembershipUser _membershipUser;
        private UserProfile _profile;

        public UserWrapper(MembershipUser membershipUser, UserProfile profile)
        {
            this._membershipUser = membershipUser;
            this._profile = profile;
        }


        public string FirstName
        {
            get { return this._profile.FirstName; }
            set { this._profile.FirstName = value; }
        }

        public string LastName
        {
            get { return _profile.LastName; }
            set { _profile.LastName = value; }
        }

        public string Email
        {
            get { return _membershipUser.Email; }
            set { _membershipUser.Email = value; }
        }

        public string Username
        {
            get { return _membershipUser.UserName; }
        }

        public DateTime LastLogin
        {
            get { return _membershipUser.LastLoginDate; }
        }

        public DateTime CreationDate
        {
            get { return _membershipUser.CreationDate; }
        }

        public override string ToString()
        {
            return Username;
        }

        public string Role
        {
            get
            {
                return Roles.GetRolesForUser(Username).First();
            }
        }
    }

}
