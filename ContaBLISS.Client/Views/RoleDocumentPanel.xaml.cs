﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Web.Security;
using System.Web.Profile;

namespace ContaBLISS.Client.Views
{
    /// <summary>
    /// Interaction logic for RoleDocumentPanel.xaml
    /// </summary>
    public partial class RoleDocumentPanel : DevExpress.Xpf.Docking.DocumentPanel
    {
        public RoleDocumentPanel()
        {
            InitializeComponent();
            DataContext = this;
            this.SavedRoles = new ObservableCollection<string>();
            LoadSavedRoles();
        }

        public string SelectedRole { get; set; }

        public ICollection<string> SavedRoles { get; set; }

        private void LoadSavedRoles()
        {
            foreach (var rol in Roles.GetAllRoles())
            {
                SavedRoles.Add(rol);
            }
        }


        private void SaveRole()
        {
            if(!string.IsNullOrEmpty(this.SelectedRole))
            {
                try
                {
                    Roles.CreateRole(SelectedRole.Replace(" ", "").Trim());
                    SavedRoles.Add(SelectedRole);
                }
                catch (System.Exception e)
                {

                    throw e;
                }

            }
        }

        private void New()
        {
            txtRole.Text = string.Empty;
            SelectedRole = string.Empty;
        }

        private void DeleteRole()
        {
            if (!string.IsNullOrEmpty(this.SelectedRole))
            {
                try
                {
                    Roles.DeleteRole(SelectedRole.Replace(" ", "").Trim());
                    SavedRoles.Remove(SelectedRole);
                }
                catch (System.Exception e)
                {

                    throw e;
                }

            }
        }

        private void btnSave_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            SaveRole();
        }

        private void btnDelete_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            DeleteRole();
        }

        private void btnNew_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            New();
        }
    }
   
}
