﻿namespace ContaBLISS.Client.ViewModels
{
    using System.Linq.Expressions;
    using System.Linq;
    using Libraries;
    using Data;
    using Data.Models;
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Collections.Generic;
    using System.Windows.Input;
    using System.Collections.ObjectModel;
    using System.Windows.Forms;

    class AccounTypeViewModel : ViewModel<AccountType>
    {
        public AccounTypeViewModel() : base(new BusinessContext<AccountType>())
        {
            this.AccountTypes = new ObservableCollection<AccountType>();
        }

        private AccountType _selectedAccountType;
        public AccountType SelectedAccountType
        {
            get
            {
                return _selectedAccountType;
            }

            set
            {
                if (value != null)
                {
                    Description = value.Description;
                    Origin = value.Origin;
                }
                else
                {
                    Description = null;
                    Origin = 0;
                }
                _selectedAccountType = value;

                NotifyPropertyChanged();
                NotifyPropertyChanged("ImValid");
                NotifyPropertyChanged("AllowModify");

            }
        }

        public ICollection<AccountType> AccountTypes { get; private set; }

        #region ViewModel properties
        private string description;
        private Origin origin;


        [Required]
        [MinLength(2, ErrorMessage = "La descripción debe tener un mínimo de {1} caracteres")]
        public string Description
        {
            get { return description; }
            set { description = value; NotifyPropertyChanged(); }
        }

        [Required]
        public Origin Origin
        {
            get { return origin; }
            set { origin = value; NotifyPropertyChanged(); }
        }


        public bool AllowModify { get { return SelectedAccountType != null; } }

        #endregion ViewModel properties

        #region ViewModel CRUD operations

        private void Create()
        {
            var type = new AccountType()
            {
                Description = this.Description,
                Origin = this.Origin,
                Status = Status.Active
            };

            try
            {
                Context.Create(type);
                AccountTypes.Add(type);
                SelectedAccountType = null;
            }
            catch (Exception ex)
            {
                ShowErrorMessage(ex.GetBaseException().Message);
            }
        }

    
        private void Update()
        {
            _selectedAccountType.Description = Description;
            _selectedAccountType.Origin = Origin;

            try
            {
                Context.Update(this.SelectedAccountType);
            }
            catch (Exception ex)
            {
                ShowErrorMessage(ex.GetBaseException().Message);
            }
            finally
            {
                this.List();
            }
        }

        private void CreateOrUpdate()
        {
            if (AllowModify)
                Update();
            else
                Create();
        }

        private void Delete()
        {

            try
            {
                Context.Delete(SelectedAccountType);
                AccountTypes.Remove(SelectedAccountType);
                SelectedAccountType = null;

            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException ex)
            {
                ShowErrorMessage(ex.GetBaseException().Message);
            }


        }

        private void List()
        {
            AccountTypes.Clear();
            SelectedAccountType = null;
            Context.GetAll().ToList().ForEach(p => { AccountTypes.Add(p); });
        }


        #endregion

        #region ICommands

        public ICommand DeleteCommand
        {
            get
            {
                return new ActionCommand(p => Delete());
            }
        }

        public ICommand CreateOrUpdateCommand
        {
            get
            {
              return new ActionCommand(p => CreateOrUpdate());
            }
        }

        public ICommand NewCommand
        {
            get
            {
                return new ActionCommand(p => { this.SelectedAccountType = null; NotifyPropertyChanged(); });
            }
        }

        public ICommand ListCommand
        {
            get
            {
                return new ActionCommand(p => List());
            }
        }

        #endregion
        public override object GetID()
        {
            throw new NotImplementedException();
        }

        protected void ShowErrorMessage(string message)
        {
            MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
