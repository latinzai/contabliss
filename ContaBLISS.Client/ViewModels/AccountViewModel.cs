﻿namespace ContaBLISS.Client.ViewModels
{
    using System.Linq;
    using Libraries;
    using Data;
    using Data.Models;
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Collections.Generic;
    using System.Windows.Input;
    using System.Collections.ObjectModel;
    using System.Windows.Forms;

    class AccountViewModel : ViewModel<Account>
    {
        public AccountViewModel() : base(new BusinessContext<Account>())
        {
            this.Accounts = new ObservableCollection<Account>();
            this.ValidGLAccounts = new ObservableCollection<Account>();
        }

        private Account _selectedAccount;
        public Account SelectedAccount
        {
            get
            {
                return _selectedAccount;
            }

            set
            {
                if (value != null)
                {
                    this.Number = value.Number;
                    this.Description = value.Description;
                    this.Type = value.Type;
                    this.AllowTransactions = value.AllowTransactions;
                    this.Balance = value.Balance;
                    this.GLAccount = value.GLAccount;
                }
                else
                {
                    this.Number = null;
                    this.Description = null;
                    this.Type = null;
                    this.AllowTransactions = false;
                    this.Balance = null;
                    this.GLAccount = null;
                }
                _selectedAccount = value;

                NotifyPropertyChanged();
                NotifyPropertyChanged("GLAccount");
                NotifyPropertyChanged("ImValid");
                NotifyPropertyChanged("AllowModify");
                NotifyPropertyChanged("AllowTransactions");
                NotifyPropertyChanged("CanHaveBalance");

            }
        }

        public ICollection<Account> Accounts { get; private set; }
        public ICollection<Account> ValidGLAccounts { get; private set; }

        #region ViewModel properties
        private string number;
        private string description;
        private Account glAccount;
        private bool allowTrans;
        private AccountType type;


        private AccountBalance balance;

        public AccountBalance Balance
        {
            get { return balance; }
            set { balance = value; NotifyPropertyChanged(); }
        }

        [Required]
        public AccountType Type
        {
            get { return type; }
            set { type = value; NotifyPropertyChanged(); ListValidGLAccounts(); NotifyPropertyChanged("GLAccount"); }
        }

        private void ListValidGLAccounts()
        {

            ValidGLAccounts.Clear();
            if (this.Type != null)
            {
                Context.GetAll().Where(g => g.AccountTypeId == Type.Id && !g.AllowTransactions && g.Number != this.Number).ToList().ForEach(g => { ValidGLAccounts.Add(g); });
            }
        }

        public bool AllowTransactions
        {
            get { return allowTrans; }
            set
            {
                allowTrans = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("AllowModify");
                NotifyPropertyChanged("CanHaveBalance");
                NotifyPropertyChanged("AllowTransactions");

                if (!value)
                    this.Balance = null;
                else
                    this.Balance = new AccountBalance();
            }
        }

        public Account GLAccount
        {
            get { return glAccount; }
            set
            {
                glAccount = value;
                NotifyPropertyChanged();
            }
        }

        [Required]
        public string Number
        {
            get { return number; }
            set { number = value; NotifyPropertyChanged(); }
        }

        [Required]
        [MinLength(2, ErrorMessage = "La descripción debe tener un mínimo de {1} caracteres")]
        public string Description
        {
            get { return description; }
            set { description = value; NotifyPropertyChanged(); }
        }



        public bool AllowModify { get { return SelectedAccount != null; } }

        public bool CanHaveBalance
        {
            get
            {
                return this.AllowTransactions && SelectedAccount == null;
            }
        }
        #endregion ViewModel properties

        #region ViewModel CRUD operations

        private void Create()
        {
            var acc = new Account()
            {
                Number = this.Number,
                Description = this.Description,
                Type = this.Type,
                GLAccount = this.GLAccount,
                AllowTransactions = this.AllowTransactions,
                Balance = this.Balance,
                Status = Status.Active
            };

            try
            {
                var toAdd = acc;
                if (acc.Type != null)
                {
                    acc.AccountTypeId = acc.Type.Id;
                    acc.Type = null;
                }

                if (acc.Balance != null && acc.Balance.AmountCurrency != null)
                {
                    acc.Balance.CurrencyId = acc.Balance.AmountCurrency.ISOCode;
                    acc.Balance.AmountCurrency = null;
                }

                Context.Create(acc);
                Accounts.Add(toAdd);
                SelectedAccount = null;
            }
            catch (Exception ex)
            {
                ShowErrorMessage(ex.GetBaseException().Message);
            }
        }


        private void Update()
        {
            SelectedAccount.Number = this.Number;
            SelectedAccount.Description = this.Description;
            SelectedAccount.GLAccount = this.GLAccount;
            SelectedAccount.Type = this.Type;
            SelectedAccount.AllowTransactions = this.AllowTransactions;
            try
            {
                //if (SelectedAccount.Type != null)
                //{
                //    SelectedAccount.AccountTypeId = SelectedAccount.Type.Id;
                //    SelectedAccount.Type = null;
                //}

                //if (SelectedAccount.Balance != null && SelectedAccount.Balance.AmountCurrency != null)
                //{
                //    SelectedAccount.Balance.CurrencyId = SelectedAccount.Balance.AmountCurrency.ISOCode;
                //    SelectedAccount.Balance.AmountCurrency = null;
                //}
                //if (SelectedAccount.GLAccount != null)
                //{
                //    SelectedAccount.GLAccountId = SelectedAccount.GLAccount.Id;
                //    SelectedAccount.GLAccount = null;
                //}


                //var accToUpdate = new Account
                //{
                //    Id = SelectedAccount.Id,
                //    Status = SelectedAccount.Status,
                //    GLAccountId = SelectedAccount.Id,
                //    AccountTypeId = SelectedAccount.AccountTypeId,
                //    Description = SelectedAccount.Description,
                //    Number = SelectedAccount.Number,
                //    AllowTransactions = SelectedAccount.AllowTransactions
                //};
                Context.Update(SelectedAccount);
            }
            catch (Exception ex)
            {
                ShowErrorMessage(ex.GetBaseException().Message);
            }
            finally
            {
                this.List();
            }
        }

        private void CreateOrUpdate()
        {
            if (AllowModify)
                Update();
            else
                Create();
        }

        private void Delete()
        {

            try
            {
                Context.Delete(SelectedAccount);
                Accounts.Remove(SelectedAccount);
                SelectedAccount = null;

            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException ex)
            {
                ShowErrorMessage(ex.GetBaseException().Message);
            }


        }

        private void List()
        {
            Accounts.Clear();
            SelectedAccount = null;
            Context.GetAll().ToList().ForEach(p => { Accounts.Add(p); });
        }


        #endregion

        #region ICommands

        public ICommand DeleteCommand
        {
            get
            {
                return new ActionCommand(p => Delete());
            }
        }

        public ICommand CreateOrUpdateCommand
        {
            get
            {
                return new ActionCommand(p => CreateOrUpdate());
            }
        }

        public ICommand NewCommand
        {
            get
            {
                return new ActionCommand(p => { this.SelectedAccount = null; NotifyPropertyChanged(); NotifyPropertyChanged("GLAccount"); });
            }
        }

        public ICommand ListCommand
        {
            get
            {
                return new ActionCommand(p => List());
            }
        }

        #endregion
        public override object GetID()
        {
            throw new NotImplementedException();
        }

        protected void ShowErrorMessage(string message)
        {
            MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
