﻿namespace ContaBLISS.Client.ViewModels
{
    using System.Linq;
    using Libraries;
    using Data;
    using Data.Models;
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Collections.Generic;
    using System.Windows.Input;
    using System.Collections.ObjectModel;
    using System.Windows.Forms;

    public class TransactionViewModel : ViewModel<Transaction>
    {
        public TransactionViewModel() : base(new BusinessContext<Transaction>())
        {
            this.Transactions = new ObservableCollection<Transaction>();
            this.CreditEntries = new ObservableCollection<Entry>();
            this.DebitEntries = new ObservableCollection<Entry>();
            this.Accounts = new ObservableCollection<Account>();
            DebitEntry = new Entry();
            CreditEntry = new Entry();

            using (var conn = new Connection())
            {
                conn.Accounts.Include("Balance.AmountCurrency").Include("Type").Where(a => a.AllowTransactions && a.Status == Status.Active).ToList().ForEach(a => { this.Accounts.Add(a); });
            }
        }

        #region ViewModel properties

        private Entry _debitEntry;
        private Entry _creditEntry;
        private Transaction _selectedTrans;
        private DateTime _date;
        private string description;
        private decimal debitAmount;
        private decimal creditAmount;

        public ICollection<Account> Accounts { get; set; }

        public decimal CreditAmount
        {
            get { return creditAmount; }
            set { creditAmount = value; CreditEntry.Amount = value; NotifyPropertyChanged(); NotifyPropertyChanged("CreditAccountHasBalance"); }
        }

        public decimal DebitAmount
        {
            get { return debitAmount; }
            set { debitAmount = value; DebitEntry.Amount = value; NotifyPropertyChanged(); NotifyPropertyChanged("DebitAccountHasBalance"); }
        }


        public Entry DebitEntry
        {
            get { return _debitEntry; }
            set { _debitEntry = value; NotifyPropertyChanged(); NotifyPropertyChanged("DebitAccountHasBalance"); }
        }


        public Entry CreditEntry
        {
            get { return _creditEntry; }
            set { _creditEntry = value; NotifyPropertyChanged(); NotifyPropertyChanged("CreditAccountHasBalance"); }
        }


        public Transaction SelectedTransaction
        {
            get
            {
                return _selectedTrans;
            }

            set
            {
                if (value != null)
                {
                    Description = value.Description;

                }
                else
                {
                    Description = null;

                }
                _selectedTrans = value;

                NotifyPropertyChanged();
                NotifyPropertyChanged("ImValid");
                NotifyPropertyChanged("AllowModify");

            }
        }

        public ICollection<Transaction> Transactions { get; private set; }

        public ICollection<Entry> DebitEntries { get; set; }
        public ICollection<Entry> CreditEntries { get; set; }


        [Required]
        [MinLength(2, ErrorMessage = "La descripción debe tener un mínimo de {1} caracteres")]
        public string Description
        {
            get { return description; }
            set { description = value; NotifyPropertyChanged(); }
        }


        [Required]
        public DateTime Date
        {
            get { return _date; }
            set { _date = value; }
        }
        
        public bool DebitAccountHasBalance
        {
            get
            {
                if (Accounts.Any() && DebitEntry.Account != null && DebitEntry.Account.Type.Origin != Origin.DB)
                    return DebitEntry.Account.Balance.Amount > this.DebitAmount;
                else
                    return true;
            }
        }

        public bool CreditAccountHasBalance
        {
            get
            {
                if (Accounts.Any() && CreditEntry.Account != null && CreditEntry.Account.Type.Origin != Origin.CR)
                    return CreditEntry.Account.Balance.Amount > this.CreditAmount;
                else
                    return true;
            }
        }

        public bool TransactionIsBalanced
        {
            get
            {
                return CreditEntries.Sum(e => e.Amount) == DebitEntries.Sum(e => e.Amount);
            }
        }
        public bool AllowModify { get { return SelectedTransaction != null; } }

        #endregion ViewModel properties

        #region ViewModel CRUD operations

        private void Create()
        {
            var curr = new Transaction()
            {
                Description = this.Description,
                ImValid = this.ImValid,
                Status = Status.Active,
                Entries = ProcessEntriesToBeSaved().ToList()
            };

            try
            {
                Context.Create(curr);
                Transactions.Add(curr);
                SelectedTransaction = null;
            }
            catch (Exception ex)
            {
                ShowErrorMessage(ex.GetBaseException().Message);
            }
        }


        private void Update()
        {

            SelectedTransaction.Description = this.Description;
            try
            {
                Context.Update(this.SelectedTransaction);
            }
            catch (Exception ex)
            {
                ShowErrorMessage(ex.GetBaseException().Message);
            }
            finally
            {
                this.List();
            }
        }

        private void CreateOrUpdate()
        {
            if (AllowModify)
                Update();
            else
                Create();
        }

        private void Delete()
        {

            try
            {
                Context.Delete(SelectedTransaction);
                Transactions.Remove(SelectedTransaction);
                SelectedTransaction = null;

            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException ex)
            {
                ShowErrorMessage(ex.GetBaseException().Message);
            }


        }

        private void List()
        {
            Transactions.Clear();
            SelectedTransaction = null;
            Context.GetAll().ToList().ForEach(p => { Transactions.Add(p); });
        }

        private void AddCredit()
        {
            if(CreditEntry.Account != null )
            {
                var entry = CreditEntry;
                entry.Origin = Origin.CR;
                entry.Currency = CreditEntry.Account.Balance.AmountCurrency;
                entry.AccountId = entry.Account.Id;
                this.CreditEntries.Add(entry);
                var acc = CreditEntry.Account;
                CreditEntry = new Entry();

                this.Accounts.Remove(acc);
                CreditAmount = 0;
                NotifyPropertyChanged();
                NotifyPropertyChanged("CreditAccountHasBalance");
                NotifyPropertyChanged("TransactionIsBalanced");
            }

        }

        private IEnumerable<Entry> ProcessEntriesToBeSaved()
        {
            var credits = from e in CreditEntries
                          select new Entry { AccountId = e.AccountId, CurrencyId = e.CurrencyId, Amount = e.Amount, Origin = e.Origin  };

            var debits = from e in CreditEntries
                          select new Entry { AccountId = e.AccountId, CurrencyId = e.CurrencyId, Amount = e.Amount, Origin = e.Origin };

            return credits.Concat(debits);
        }

        private void AddDebit()
        {
            if(DebitEntry.Account != null)
            {
                var entry = DebitEntry;
                entry.Origin = Origin.DB;
                entry.Currency = DebitEntry.Account.Balance.AmountCurrency;
                entry.AccountId = entry.Account.Id;
                this.DebitEntries.Add(entry);
                var acc = DebitEntry.Account;
                DebitEntry = new Entry();
                this.Accounts.Remove(acc);
                DebitAmount = 0;
                NotifyPropertyChanged();
                NotifyPropertyChanged("DebitAccountHasBalance");
                NotifyPropertyChanged("TransactionIsBalanced");
            }


        }
        #endregion

        #region ICommands

        public ICommand DeleteCommand
        {
            get
            {
                return new ActionCommand(p => Delete());
            }
        }

        public ICommand CreateOrUpdateCommand
        {
            get
            {
                return new ActionCommand(p => CreateOrUpdate());
            }
        }

        public ICommand NewCommand
        {
            get
            {
                return new ActionCommand(p => { this.SelectedTransaction = null; NotifyPropertyChanged(); });
            }
        }

        public ICommand ListCommand
        {
            get
            {
                return new ActionCommand(p => List());
            }
        }

        public ICommand AddCreditCommand
        {
            get
            {
                return new ActionCommand(p => AddCredit());
            }
        }

        public ICommand AddDebitCommand
        {
            get
            {
                return new ActionCommand(p => AddDebit());
            }
        }
        //public ICommand UpdateBalances
        //{
        //    get
        //    {
        //        return new ActionCommand(p => { NotifyPropertyChanged("DebitAccountHasBalance"); NotifyPropertyChanged("CreditAccountHasBalance"); });
        //    }
        //}
        #endregion
        public override object GetID()
        {
            throw new NotImplementedException();
        }

        protected override string OnValidate(string propertyName)
        {
            if (propertyName == "DebitAmount" && !this.DebitAccountHasBalance)
                return "La cuenta a debitar no tiene balance suficiente";
            if (propertyName == "CreditAmount" && !this.CreditAccountHasBalance)
                return "La cuenta a acreditar no tiene balance suficiente";
            if (!this.TransactionIsBalanced)
                return string.Format("Los balances son diferentes {0} ≠ {1}", DebitEntries.Sum(e => e.Amount), CreditEntries.Sum(e => e.Amount));
            
            return base.OnValidate(propertyName);
        }
        protected void ShowErrorMessage(string message)
        {
            MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
