﻿namespace ContaBLISS.Client.ViewModels
{
    using System.Linq;
    using Libraries;
    using Data;
    using Data.Models;
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Collections.Generic;
    using System.Windows.Input;
    using System.Collections.ObjectModel;
    using System.Windows.Forms;

    class CurrencyViewModel : ViewModel<Currency>
    {
        public CurrencyViewModel() : base(new BusinessContext<Currency>())
        {
            this.Currencies = new ObservableCollection<Currency>();
        }

        private Currency _selectedCurrency;
        public Currency SelectedCurrency
        {
            get
            {
                return _selectedCurrency;
            }

            set
            {
                if (value != null)
                {
                    ISOCode = value.ISOCode;
                    Description = value.Description;
                    Simbol = value.Simbol;

                }
                else
                {
                    ISOCode = null;
                    Description = null;
                    Simbol = null;

                }
                _selectedCurrency = value;

                NotifyPropertyChanged();
                NotifyPropertyChanged("ImValid");
                NotifyPropertyChanged("AllowModify");

            }
        }

        public ICollection<Currency> Currencies { get; private set; }

        #region ViewModel properties
        private string isoCode;
        private string description;
        private string simbol;

        [Required]
        [StringLength(3, MinimumLength = 3, ErrorMessage = "El código debe ser de {1} caracteres")]
        public string ISOCode
        {
            get { return isoCode; }
            set { isoCode = value; NotifyPropertyChanged(); }
        }


        [Required]
        [MinLength(2, ErrorMessage = "La descripción debe tener un mínimo de {1} caracteres")]
        public string Description
        {
            get { return description; }
            set { description = value; NotifyPropertyChanged(); }
        }

        [Required]
        [MaxLength(6, ErrorMessage = "El campo no debe exceder los {1} caracteres")]
        public string Simbol
        {
            get { return simbol; }
            set { simbol = value; NotifyPropertyChanged(); }
        }

        public bool AllowModify { get { return SelectedCurrency != null; } }

        #endregion ViewModel properties

        #region ViewModel CRUD operations

        private void Create()
        {
            var curr = new Currency()
            {
                ISOCode = this.ISOCode,
                Description = this.Description,
                Simbol = this.Simbol,
                ImValid = this.ImValid,
                Status = Status.Active
            };

            try
            {
                Context.Create(curr);
                Currencies.Add(curr);
                SelectedCurrency = null;
            }
            catch (Exception ex)
            {
                ShowErrorMessage(ex.GetBaseException().Message);
            }
        }

    
        private void Update()
        {

            SelectedCurrency.ISOCode = this.ISOCode;
            SelectedCurrency.Description = this.Description;
            SelectedCurrency.Simbol = this.Simbol;
            try
            {
                Context.Update(this.SelectedCurrency);
            }
            catch (Exception ex)
            {
                ShowErrorMessage(ex.GetBaseException().Message);
            }
            finally
            {
                this.List();
            }
        }

        private void CreateOrUpdate()
        {
            if (AllowModify)
                Update();
            else
                Create();
        }

        private void Delete()
        {

            try
            {
                Context.Delete(SelectedCurrency);
                Currencies.Remove(SelectedCurrency);
                SelectedCurrency = null;

            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException ex)
            {
                ShowErrorMessage(ex.GetBaseException().Message);
            }


        }

        private void List()
        {
            Currencies.Clear();
            SelectedCurrency = null;
            Context.GetAll().ToList().ForEach(p => { Currencies.Add(p); });
        }


        #endregion

        #region ICommands

        public ICommand DeleteCommand
        {
            get
            {
                return new ActionCommand(p => Delete());
            }
        }

        public ICommand CreateOrUpdateCommand
        {
            get
            {
              return new ActionCommand(p => CreateOrUpdate());
            }
        }

        public ICommand NewCommand
        {
            get
            {
                return new ActionCommand(p => { this.SelectedCurrency = null; NotifyPropertyChanged(); });
            }
        }

        public ICommand ListCommand
        {
            get
            {
                return new ActionCommand(p => List());
            }
        }

        #endregion
        public override object GetID()
        {
            throw new NotImplementedException();
        }

        protected override string OnValidate(string propertyName)
        {

            if (!AllowModify && propertyName == "ISOCode" && Currencies.Where(c => c.ISOCode == this.ISOCode).Any())
                return "La ya moneda existe";

            return base.OnValidate(propertyName);
        }

        protected void ShowErrorMessage(string message)
        {
            MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
