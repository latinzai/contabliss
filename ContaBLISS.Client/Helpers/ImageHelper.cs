﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ContaBLISS.Client.Helpers
{
    public static class ImageHelper
    {
        public static System.Windows.Controls.Image ToWPFImage(this System.Drawing.Image image, Stretch stretch)
        {
            System.Windows.Controls.Image img = new System.Windows.Controls.Image();

            System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(image);
            IntPtr hBitmap = bmp.GetHbitmap();
            ImageSource WpfBitmap = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(hBitmap, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());

            img.Source = WpfBitmap;
            img.Width = image.Width;
            img.Height = image.Height;
            img.Stretch = stretch;
            return img;
        }
    }
}
