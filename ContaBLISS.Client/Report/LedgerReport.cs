﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using ContaBLISS.Data;
using System.Linq;
namespace ContaBLISS.Client.Report
{
    public partial class LedgerReport : DevExpress.XtraReports.UI.XtraReport
    {
        public LedgerReport()
        {
            InitializeComponent();
        }

        private decimal totalBalance = 0;
        private decimal totalCredit = 0;
        private decimal totalDebit = 0;

        private void lblBalance_SummaryGetResult(object sender, SummaryGetResultEventArgs e)
        {
            Origin origin;
            using (var conn = new Connection())
            {
               string currentAccNo = GetCurrentColumnValue("Number").ToString();
               origin = conn.Accounts.Where(a => a.Number == currentAccNo).First().Type.Origin;
            }
            if (origin == Origin.DB)
                totalBalance = totalDebit - totalCredit;
            else
                totalBalance = totalCredit - totalDebit;
            e.Result = totalBalance;
            e.Handled = true;
        }

        private void lblBalance_SummaryReset(object sender, EventArgs e)
        {
            totalCredit = 0;
            totalDebit = 0;
            totalBalance = 0;
            //   totalBalance += decimal.Parse("DebitAc")
        }

        private void lblBalance_SummaryRowChanged(object sender, EventArgs e)
        {
            totalCredit += Convert.ToDecimal(GetCurrentColumnValue("CreditAmount"));
            totalDebit += Convert.ToDecimal(GetCurrentColumnValue("DebitAmount"));
        }
    }
}
